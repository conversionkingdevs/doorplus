function updateClasses() {
    jQuery('.wpsl-store-location .store_add').removeClass('col-md-6').addClass('col-md-9');
    jQuery('.wpsl-store-location .store_img').removeClass('col-md-6').addClass('col-md-3');
}

function addPhoneNumbers () {
    jQuery('.store_add').each(function () {
        jQuery(this).find('p:eq(0)').append('<div class="optPhoneNumber"></div>');
        jQuery(this).find('.optPhoneNumber').load(jQuery(this).find('>p>strong>a').attr('href')+" .store_add > a", function(){
            this;
        });
    })
}

function addShowOpeningHoursFunctionality() {
    jQuery('.opt_OpeningHours').click(function (e) { 
        e.preventDefault();
        var item = jQuery(this).closest('.wpsl-store-location').find('.opt_OpeningHoursContainer');
        jQuery(this).addClass('opt_Hide');
        if (item.hasClass('opt_Hide')) {
            item.removeClass('opt_Hide');
        } else {
            item.addClass('opt_Hide');
        }
    });
}

function addOpeningHoursHTML() {
    jQuery('.store_add').append('<div class="opt_OpeningHoursContainer opt_Hide"><p class="opt_Title"><strong>Hours:</strong><ul><li><div class="opt_Day">Monday</div><div>9:00 AM - 5:30 PM</div></li><li><div class="opt_Day">Tuesday</div><div>9:00 AM - 5:30 PM</div></li><li><div class="opt_Day">Wednesday</div><div>9:00 AM - 5:30 PM</div></li><li><div class="opt_Day">Thursday</div><div>9:00 AM - 5:30 PM</div></li><li><div class="opt_Day">Friday</div><div>9:00 AM - 5:30 PM</div></li><li><div class="opt_Day">Saturday</div><div>9:00 AM - 4:00 PM</div></li><li><div class="opt_Day">Sunday</div><div>10:00 AM - 4:00 PM</div></li></ul></div>');
    addShowOpeningHoursFunctionality();
}

function updateButtons() {
    jQuery('.wpsl-view-more-wrap a[title="View Details"]').parent().remove();
    jQuery('.wpsl-direction-wrap').each(function () {
        jQuery(this).after('<div class="wpsl-view-more-wrap opt_OpeningHours"><a href="#" title="Opening Hours">Opening Hours</a></div>');
    })
    jQuery('.store_add > div').addClass('opt_Button');
    jQuery('.store_add').each(function () {
        jQuery(this).find('.opt_Button').wrapAll('<div class="opt_ButtonContainer"></div>');
    });
    addOpeningHoursHTML();
}

function start() {
    updateClasses();
    updateButtons();
    addPhoneNumbers();
}


function watcher(loadingIdentifier, callback){
    (function(open) {
        XMLHttpRequest.prototype.open = function(m, u, a, us, p) {
            this.addEventListener('readystatechange', function() {
                try{
                    if(this.responseURL.indexOf(loadingIdentifier) >= 0 && this.readyState == 4) {
                        callback(this.response);
                    }
                } catch(e){
                }
            }, false);
            open.call(this, m, u, a, us, p);
        };
    })(XMLHttpRequest.prototype.open);
}

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

watcher('admin-ajax.php?action=store', function (res) {
        res = JSON.parse(res);
        console.log(res);
    if (res.length == 0) {
        console.log('nothing');
    } else {
        defer(start, '.wpsl-directions');
    }
});