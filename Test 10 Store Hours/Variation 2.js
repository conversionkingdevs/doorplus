function updateClasses() {
    jQuery('.wpsl-store-location .store_img').removeClass('col-md-6').addClass('col-md-10');
}
function addShowOpeningHoursFunctionality() {
    jQuery('.opt_OpeningHours').click(function (e) { 
        e.preventDefault();
        var item = jQuery(this).closest('.wpsl-store-location').find('.opt_OpeningHoursContainer');
        jQuery(this).addClass('opt_VisbilityHidden');
        if (item.hasClass('opt_Hide')) {
            item.removeClass('opt_Hide');
        } else {
            item.addClass('opt_Hide');
        }
    });
}
function addReviewHTML() {
    jQuery('.opt_PContainer').append('<img src="//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/7f7b914ba1a7c5cfe517985f3233f170_review_desktop_%281%29.png" />');
}
function addOpeningHoursHTML() {
    jQuery('.opt_OpeningHoursContainer').append('<p class="opt_Title"><strong>Hours:</strong><ul><li><div class="opt_Day">Monday</div><div>9:00 AM - 5:30 PM</div></li><li><div class="opt_Day">Tuesday</div><div>9:00 AM - 5:30 PM</div></li><li><div class="opt_Day">Wednesday</div><div>9:00 AM - 5:30 PM</div></li><li><div class="opt_Day">Thursday</div><div>9:00 AM - 5:30 PM</div></li><li><div class="opt_Day">Friday</div><div>9:00 AM - 5:30 PM</div></li><li><div class="opt_Day">Saturday</div><div>9:00 AM - 4:00 PM</div></li><li><div class="opt_Day">Sunday</div><div>10:00 AM - 4:00 PM</div></li></ul>');
    addShowOpeningHoursFunctionality();
}
function updateButtons() {
    jQuery('.wpsl-view-more-wrap a[title="View Details"]').parent().remove();
    jQuery('.wpsl-direction-wrap').each(function () {
        jQuery(this).after('<div class="wpsl-view-more-wrap opt_OpeningHours"><a href="#" title="Opening Hours">Opening Hours</a></div>');
    })
    jQuery('.store_add > div').addClass('opt_Button');
    jQuery('.wpsl-store-location').each(function () {
        jQuery(this).find('img').wrapAll('<div class="opt_Image"></div>');
        jQuery(this).find('.store_img').append('<div class="opt_Container"></div><div class="opt_OpeningHoursContainer opt_Hide"></div>');
        jQuery(this).find('.opt_Image').appendTo(jQuery(this).find('.opt_Container'));
        jQuery(this).find('.store_img .opt_Container').append('<div class="opt_PContainer"></div>');
        jQuery(this).find('.store_add > p').appendTo(jQuery(this).find('.opt_PContainer'));
        jQuery(this).append('<div class="col-md-4 opt_ButtonContainer padd-left-15"></div>');
        jQuery(this).find('.opt_Button').appendTo(jQuery(this).find('.opt_ButtonContainer'));
    });
    addReviewHTML();
    jQuery('.store_add').remove();
    addOpeningHoursHTML();
}
function start () {
    updateClasses();
    updateButtons();
}
function watcher(loadingIdentifier, callback){
    (function(open) {
        XMLHttpRequest.prototype.open = function(m, u, a, us, p) {
            this.addEventListener('readystatechange', function() {
                try{
                    if(this.responseURL.indexOf(loadingIdentifier) >= 0 && this.readyState == 4) {
                        callback(this.response);
                    }
                } catch(e){
                }
            }, false);
            open.call(this, m, u, a, us, p);
        };
    })(XMLHttpRequest.prototype.open);
}
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
watcher('admin-ajax.php?action=store', function (res) {
        res = JSON.parse(res);
    if (res.length == 0) {
        console.log('nothing');
    } else {
        defer(start, '.wpsl-directions');
    }
});
