/* CUSTOM CODE */
function defer(method, selector) {
    if (window.jQuery) {
      if (jQuery(selector).length > 0) {
        method();
        return;
      }
    }
    setTimeout(function() { defer(method, selector); }, 50);
  }
function deferCarousel(method) {
      if (jQuery().allinone_carousel && jQuery().swipe) {
          method();
          return;
      }
      setTimeout(function() { deferCarousel(method); }, 50);
  }
// Load sliders
defer(function() {
      var s = document.createElement('script');
      var jswipe = document.createElement('script')
      var l = document.createElement('link');
      s.src = '//www.responsivejqueryslider.com/js/allinone_carousel.js';
      jswipe.src = '//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.19/jquery.touchSwipe.js';
      s.async = true;
      jswipe.async = true;
      l.href = '//www.responsivejqueryslider.com/allinone_carousel.css';
      l.rel = 'stylesheet';
      l.id = 'carousel-css'
      document.querySelector('head').appendChild(s);
      document.querySelector('head').appendChild(jswipe);
      document.querySelector('head').appendChild(l);
  }, 'head');
function servicesLayout(items, appendTo) {
      jQuery('<div class="opt-services row"></div>').appendTo(appendTo);
      for (var x = 0, lenX = items.length; x < lenX; x++) {
          jQuery('<div class="col-xs-6 col-md-3"><div class="opt-item"><div class="opt-image"><img src="'+items[x].image+'"></div><div class="opt-cornerRibbon"><span>'+items[x].text+'</span></div></div></div>').appendTo(appendTo + ' .opt-services');
      }
  }
function enquireStoreLocationLayout(appendTo) {
      jQuery('<div class="opt-enquireStoreLocation"><img src="https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/e6fc59b5f8931f881cccd541dc57c800_contact_banner_bg.png" class="opt-imagebanner"><div class="opt-contentBanner"></div></div>').appendTo(appendTo);
      jQuery('<div class="opt-mail"><img src="https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/813026d37469fd551eefd70699d7efb1_envelope.png"></div>').appendTo(appendTo + ' .opt-enquireStoreLocation .opt-contentBanner');
      jQuery('<div class="opt-buttons"><a href=""><img src="https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/6fafee24427bb090a7c9bc415aeb5dc9_enquire_now.png" alt="Enquire Now"></a><a href="tel:1800907622"><img src="https://i.imgur.com/7BHkZHb.png" /> Or call 1800 907 622</a></div>').appendTo(appendTo + ' .opt-enquireStoreLocation .opt-contentBanner');
      jQuery('<div class="opt-location"><img src="https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/c36299542eb449c5b3154d51aaf46e2c_nav_icon_from_contact_banner.png"></div>').appendTo(appendTo + ' .opt-enquireStoreLocation .opt-contentBanner');
      jQuery('<div class="opt-storeLocator"><a href="https://www.doorsplus.com.au/store-locator/"><img src="https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/ea8ef9e0b3f195074890f353cadbeb48_store_locator.png" alt="Store Locator"><span>Find your nearest showroom</span></a></div>').appendTo(appendTo + ' .opt-enquireStoreLocation .opt-contentBanner');
  }
function section1() {
      var images = [
          'https://i.imgur.com/toiKGgS.jpg',
          'https://i.imgur.com/dCAbK4D.jpg',
          'https://i.imgur.com/VSyX8aY.jpg',
          'https://i.imgur.com/DjBOFoS.jpg',
          'https://i.imgur.com/dK9NVYg.jpg'
      ];
      jQuery('<div class="col-md-12 opt-section1"></div>').appendTo('.materials_sub > .row');
      jQuery('<h1><img src="https://i.imgur.com/kNymZMf.png" alt="The Guardian Range 2 in 1 Door" /></h1><div class="hide ck-flickable-slider"><ul class="allinone_carousel_list"></ul></div>').appendTo('.opt-section1');
      for(idx in images) {
          jQuery("<li><img src='" + images[idx] + "' /></li>").appendTo('.opt-section1 .ck-flickable-slider .allinone_carousel_list');
      }
      deferCarousel(function() {
          jQuery('.opt-section1 .ck-flickable-slider').removeClass('hide').allinone_carousel({
              skin: 'charming',
              responsive:true,
              autoPlay: false,
              enableTouchScreen: true,
              showCircleTimer: false,
              showCircleTimerIE8IE7: false,
              resizeImages:true,
              autoHideBottomNav:false,
              showNavArrows: true,
              autoHideNavArrows: false,
              showElementTitle:false,
              elementsHorizontalSpacing: 120,
              verticalAdjustment:50,
              showPreviewThumbs:false,
              //easing:'easeOutBounce',
              numberOfVisibleItems:4,
              nextPrevMarginTop: 315,
              playMovieMarginTop:0,
              bottomNavMarginBottom:-10,
              height: 650,
              width: 1950
          });
      });
  }
function section2() {
      var section2 = {
          title: 'Guardian 2 in 1 - An Entry & Screen Door In One!',
          subtitle: 'Have An Idea? Let Us Surprise you with our Guardian 2 in 1 Range.',
          description: 'Our innovative Guardian 2 in 1 doors incorporate a panel of SafeGlass™ that can be opened from the inside, and an integrated stainless steel mesh screen to protect you from the outside.',
          steps: [
              {
                  text: '1 on 1 Consultation',
                  image: 'https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/6aa070ace098e6d58d1d9d809aee1236_1_to_1_consultation.png'
              }, {
                  text: 'Visit A Showroom For A Free Measure & Quote',
                  image: 'https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/a552363c12d697302d9c4abd709e9acf_measure_%26_quote.png'
              }, {
                  text: 'Home Delivery',
                  image: 'https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/94e9eccf7f6c81328ac212ed844b66b3_home_delivery.png'
              }, {
                  text: 'Expert Installation',
                  image: 'https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/012bf7075db0e9e79c954cf1d808638a_expert_installation.png'
              }
          ]
      };
      jQuery('<div class="col-md-12 opt-section2"></div>').appendTo('.materials_sub > .row');
      jQuery('<p class="opt-title">'+section2.title+'</p>').appendTo('.opt-section2');
      jQuery('<p class="opt-subtitle">'+section2.subtitle+'</p>').appendTo('.opt-section2');
      jQuery('<p class="opt-description">'+section2.description+'</p>').appendTo('.opt-section2');
      servicesLayout(section2.steps, '.opt-section2');
      enquireStoreLocationLayout('.opt-section2');
  }
function section3() {
      jQuery('<div class="col-md-12 opt-section3"></div>').appendTo('.materials_sub > .row');
      var content =   '<h2 class="mobile subtitle">Innovation. Style. Safety</h2>' +
                      '<img src="https://i.imgur.com/wlRhhxr.jpg" />'+
                      '<div>'+
                          '<h2 class="subtitle">Innovation. Style. Safety</h2>' +
                          '<ul>' +
                              '<li>Feel secure, see who is at your door before unlocking it.</li>' +
                              '<li>Easy to open & use with only one key! </li>' +
                              '<li>Fresh air & safety! Let the breeze flow while the door is locked</li>' +
                              '<li>Have Pets at Home? There are Pet Door Options with the Guardian 2 in 1 Range</li>' +
                          '</ul>' +
                      '</div>';
      jQuery(content).appendTo('.opt-section3');
  }
function section4() {
      var section4 = {
          reviews: [
              {
                  title: 'Hear From Some Happy Guardian 2 in 1 Customers',
                  review: '“We have the Guardian 2 in 1 door and love it :) Little confusing for our dog at the moment but i’m sure he will get the gist of it :D A few friends have commented on what a good idea it is.”',
                  reviewer: 'Leonie Hearne',
                  date: 'October 2018',
                  picture: 'https://www.doorsplus.com.au/wp-content/plugins/easy-testimonials/include/assets/img/mystery-person.png'
              }, {
                  title: 'Hear From Some Happy Guardian 2 in 1 Customers',
                  review: '“I have visited the store looking for an external door and Michael had shown me around to find the door that best suited my houes the two in one guardian door.I am very happy that Michael had explained everything to me and i recommend doorsplus to my family and friends. Thank you for the service.”',
                  reviewer: 'Davy',
                  date: 'October 2018',
                  picture: 'https://s.productreview.com.au/users/images/t/100x100/4ca22c25655c5ecee085f3085fb65d30.jpg'
              }
          ],
          banner: 'https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/8f9fa8f9139f025c7439ed1b6d219e85_2in1_banner_updated.jpg'
      };
      jQuery('<div class="col-md-12 opt-section4"></div>').appendTo('.materials_sub > .row');
      jQuery('<div class="opt-reviews"><ul class="slides"></ul></div>').appendTo('.opt-section4');
      for (var x = 0, lenX = section4.reviews.length; x < lenX; x++) {
          jQuery('<li></li>').appendTo('.opt-section4 .opt-reviews ul');
          jQuery('<p class="opt-title">'+section4.reviews[x].title+'</p>').appendTo('.opt-section4 .opt-reviews ul li:eq('+x+')');
          jQuery('<div class="opt-review"></div>').appendTo('.opt-section4 .opt-reviews ul li:eq('+x+')');
          jQuery('<img src="'+section4.reviews[x].picture+'">').appendTo('.opt-section4 .opt-reviews ul li:eq('+x+') .opt-review');
          jQuery('<p class="opt-text">'+section4.reviews[x].review+'</p><p class="opt-reviewer"><span>'+section4.reviews[x].reviewer+'</span><span>'+section4.reviews[x].date+'</span></p>').appendTo('.opt-section4 .opt-reviews ul li:eq('+x+') .opt-review');
      }
      var firstMobileResize = false;
      function getScript(src, callback) {
          var s = document.createElement('script');
          s.src = src;
          s.async = true;
          s.onreadystatechange = s.onload = function() {
              if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
                  callback.done = true; callback();
              }
          };
          document.querySelector('head').appendChild(s);
      }
      function mobileVersion(){
          if (jQuery(".opt-reviews.flexslider").length === 0) {
              jQuery(".opt-reviews").addClass('flexslider');
              jQuery(".flexslider").flexslider({
                  controlNav: true,
                  directionNav: true,
                  pauseOnAction: false,
                  slideshow: false
              });
              jQuery('<i class="fa fa-arrow-right"></i>').appendTo('.flex-direction-nav li.flex-nav-prev a');
              jQuery('<i class="fa fa-arrow-left"></i>').appendTo('.flex-direction-nav li.flex-nav-next a');
              jQuery('.flex-direction-nav li a').text('');
          }
      }
      function desktopVersion() {
          if (jQuery(".opt-reviews.flexslider").length > 0) {
              jQuery('.opt-reviews').removeClass('flexslider');
          }
      }
      function checkScreenSize() {
          if (jQuery(window).width() <= 991){
              if (!firstMobileResize) {
                  getScript('https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider-min.js', mobileVersion);
                  jQuery('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css" />');
                  firstMobileResize = true;
              } else {
                  mobileVersion();
              }
          } else {
              desktopVersion();
          }
      }
      checkScreenSize();
      jQuery('<div class="opt-banner"><img src="'+section4.banner+'"></div>').appendTo('.opt-section4');
      $(window).resize(checkScreenSize);
  }
function section5() {
      var images = [
          'https://i.imgur.com/brSgfg8.jpg',
          'https://i.imgur.com/bO9wo7s.jpg',
          'https://i.imgur.com/JBH8dw0.jpg',
          'https://i.imgur.com/ZyJLTWg.jpg',
          'https://i.imgur.com/eWKCtnN.jpg'
      ];
      jQuery('<div class="col-md-12 opt-section5"></div>').appendTo('.materials_sub > .row');
      jQuery('<h2 class="subtitle">See what your neighbours are doing with the Guardian 2 in 1 range!</h2><div class="hide ck-flickable-slider"><ul class="allinone_carousel_list"></ul></div>').appendTo('.opt-section5');
      for(idx in images) {
          jQuery("<li><img src='" + images[idx] + "' /></li>").appendTo('.opt-section5 .ck-flickable-slider .allinone_carousel_list');
      }
      deferCarousel(function() {
          jQuery('.opt-section5 .ck-flickable-slider').removeClass('hide').allinone_carousel({
              skin: 'charming',
              responsive:true,
              autoPlay: false,
              enableTouchScreen: true,
              showCircleTimer: false,
              showCircleTimerIE8IE7: false,
              resizeImages:true,
              autoHideBottomNav:false,
              showNavArrows: true,
              autoHideNavArrows: false,
              showElementTitle:false,
              elementsHorizontalSpacing: 120,
              verticalAdjustment:50,
              showPreviewThumbs:false,
              //easing:'easeOutBounce',
              numberOfVisibleItems:4,
              nextPrevMarginTop: 221,
              playMovieMarginTop:0,
              bottomNavMarginBottom:-10,
              height: 480,
              width: 1950
          });
      });
  }
function section6() {
      var section6 = {
          title: 'Guardian 2 in 1 Without the Fuss!',
          subtitle: 'We can include all that you need in the Doors Plus Experience.',
          items: [
              {
                  text: 'Hardware',
                  image: 'https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/2eb18a53ca38a202291c013bffec9005_hardware.png'
              }, {
                  text: 'Finishing',
                  image: 'https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/602157f5e7ce31e2f17140bcb37edfe3_finishing.png'
              }, {
                  text: 'Delivery',
                  image: 'https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/f8e1e4a1a4ecbcc8ec18cf46ccc70008_delivery.png'
              }, {
                  text: 'Installation',
                  image: 'https://useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/8fb846c92f176611dc8939dbe354c81d_installation.png'
              }
          ]
      };
      jQuery('<div class="col-md-12 opt-section6"></div>').appendTo('.materials_sub > .row');
      jQuery('<p class="opt-title">'+section6.title+'</p>').appendTo('.opt-section6');
      jQuery('<p class="opt-subtitle">'+section6.subtitle+'</p>').appendTo('.opt-section6');
      servicesLayout(section6.items, '.opt-section6');
  }
function section7(video) {
      jQuery('<div class="col-md-12 opt-section7"><h2 class="subtitle">Guardian 2 in 1 Review</h2></div>').appendTo('.materials_sub > .row');
      jQuery(video).appendTo(".opt-section7");
      enquireStoreLocationLayout('.opt-section7');
      var description = "<div class='description'>"+
                          '<p>Our Guardian 2 in 1 external doors are an innovation in screen door safety and functionality. Ideal for front and back entrances, laundry, garage and side door solutions, the revolutionary design offers an external and screen door in one, combining safety, convenience and flexibility.</p>'+
                          '<p>We offer a solid timber version or HMR MDF options, each housing a translucent panel of Doors Plus SafeGlass™ and an integrated stainless steel mesh screen. There is a range of designs to choose from, including partial and full length glass panels, so your new external doors are both functional and stylish. Our Guardian doors are available in a single or double configuration, with optional side panels.</p>'+
                          '<p>This outstanding screen door solution offers superior protection against the elements, and against unwanted visitors, without blocking out natural light and fresh air. From the inside, you can easily open the glass panel while keeping the door firmly locked and the screen in place. This allows you to let in a breeze, without the bugs and debris that can come along with it, and without leaving your home exposed to uninvited guests. From the outside, you can enjoy the convenience of having just one door to open when your hands are full!</p>'+
                          '<p>Dual magnetic flexible seals make your Guardian doors waterproof and fully insulated against draughts, so you get all the benefits of a robust and solid external door, with the flexibility of the integrated screen. The water shed bar prevents water from running down the glass and pooling at the bottom of your doors.</p>'+
                          '<p>The Guardian range combines marine grade 316 stainless steel and our exclusive Doors Plus SafeGlass™, which doesnt shatter if broken, so you can be sure that you are making the best choice for the safety of your family. You can even integrate a cat or dog door so that your pets can come and go as they please, even when the screen is closed.</p>'+
                          '<p>Our Guardian 2 in 1 doors really do give you the best of both worlds, and you dont have to compromise on style. Your doors can be painted white for the MDF version or finished with a dark maple stain for the Solid Timber version, or we can provide them raw or pre-primed, to be finished in the colour of your choice. We will even arrange installation for you, so your new, 2 in 1 doors will be in place before you know it.</p>'+
                        "</div>";
      var showroom = '<div class="showroom">'+
                          '<h2 class="subtitle">Visit a Showroom Near You</h2>'+
                          '<p>Our qualified staff are available, 7 days a week, to assist you in choosing the right doors for your home. Once you have made your selection, you can arrange for a FREE measure and quote.</p>'+
                          '<p><a href="https://www.doorsplus.com.au/store-locator/">Click here</a> to find a store closest to you.</p>'+
                      '</div>';
      jQuery(description).appendTo(".opt-section7");
      jQuery(showroom).appendTo(".opt-section7");
  }
defer(function() {
      var video = jQuery(".part_right div:first").clone(true);
      jQuery('.materials_sub .row:eq(0), .sub_page_banner').remove();
      jQuery('<div class="row content"></div>').appendTo('.materials_sub');
      section1();
      section2();
      section3();
      section4();
      section5();
      section6();
      section7(video);
  }, '.materials_sub .row:eq(0)');
