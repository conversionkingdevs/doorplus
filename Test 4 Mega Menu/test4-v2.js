

$ = jQuery.noConflict();

$.fn.adjustHeight = function() {
    var maxHeightFound = 0;
    this.css('min-height','1px');
    this.each(function() {
        if( jQuery(this).innerHeight() > maxHeightFound ) {
            maxHeightFound = jQuery(this).innerHeight();
        }
    });
    this.css('min-height',maxHeightFound);
};

$('body').addClass('test4');

var opt2132 = {
        1 : {"title":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-french-solid-timber-with-glass/", "image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/5f9e1ee54f8c53f034d4b968a2a35a4b_front_french.png"},
        2 : {"title":"Guardian 2in1 Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-guardian-2in1/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/d5fd3e8fd9b2c4d5ac16b465958e7ffe_front_guardian.png"},
        3 : {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-hinged/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/c0f020b223ac9661ad8a6fde17fd5071_front_hinged.png"},
        4 : {"title":"Large Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-large/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/1f3e6f7fbf7c80c8f532397ae6e26cf5_front_wide.png"}
    },
    opt1916 = {
        1 : {"title":"Bi Fold Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-bi-fold/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/bddd3129aac66ba463e12ae357159069_external_folding.png"},
        2 : {"title":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-french-solid-timber-with-glass/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/b6095ad8e8e3fa4e40ef4eb7c7fb8296_external_french.png"},
        3 : {"title":"Guardian 2in1 Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-guardian-2in1/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/0cc1041d6cdaf99f2873f2774ca857b3_external_guardian.png"},
        4 : {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-hinged/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/3c241e6940ddafd034711f6f17951a13_external_hinged.png"},
        5 : {"title":"Sliding & Stacking Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-sliding-stacking/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/f8addec112f4498807d58ee14d74f522_external_sliding___stacking.png"}
    },
    opt2133 = {
        1 : {"title":"Bi Fold Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-bi-fold/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/52b44fd999291a34e7fcdac0cbe9225a_internal_bifold.png"},
        2 : {"title":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-french-solid-timber-with-glass/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/4c8e974fdb3631c122057a3301ace85c_internal_french.png"},
        3 : {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-hinged/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/8c0f9b04995b501d6f4979e4fb130345_internal_hinged.png"},
        4 : {"title":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-sliding/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/69888938ad3d93a51fc231b54c32f31e_internal_sliding.png"}
    },
    opt2072 = {
        1 : {"title":"Bi - Fold Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-bi-fold/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/d6f8f96e090819ec0ca49bad3374598d_zone_living_folding.png"},
        2 : {"title":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-french/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/2ec2430815f34c1578bf0c4801bb88a5_zone_living_french_folding.png"},
        3 : {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-hinged/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/c2f8d539d30e54a1bf9c3b8a451c110b_zone_living_hinged.png"},
        4 : {"title":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-sliding-stacking/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/92cd0e2055bdec12298a7aef09dd8bad_zone_living_sliding_stacking.png"}
    },
    opt2135 = {
        1 : {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-hinged/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/615289e56bb3128849893daee4b7efa7_ultrasafe_hinged_wide.png"},
        2 : {"title":"Large Doors","link":"https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-large/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/f10811c7ffb1cc65357efdd81d27583b_ultrasafe_screen_hinged.png"},
        3 : {"title":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-sliding/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/028674fc680667daf70a9bc0e8847b39_ultrasafe_sliding.png"}
    },
    opt2134 = {
        1 : {"title":"Hideaway Retractable Screens Doors","link":"https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-hideaway-retractable-screens/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/db15146e0bb26dc6f276846b349b50f6_hideaway_screen.png"},
        2 : {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-hinged/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/4f201d5d0947f501426d9f2c21a2933c_aluminium_screen_hinged.png"},
        3 : {"title":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-sliding/","image":"//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/ee01e31aa132ecdecc8308b8df0fc50b_aluminium_screen_sliding.png"}
    };



$(".nav_area ul#menu-main_menu > li").each(function(){
    $(this).find('a').wrapInner('<span />');
});

$("#menu-item-2132").append('<div class="megamenu-wrap"><div class="megamenu optmenu-item-2132" /></div>');
$("#menu-item-1916").append('<div class="megamenu-wrap"><div class="megamenu optmenu-item-1916" /></div>');
$("#menu-item-2133").append('<div class="megamenu-wrap"><div class="megamenu optmenu-item-2133" /></div>');
$("#menu-item-2072").append('<div class="megamenu-wrap"><div class="megamenu optmenu-item-2072" /></div>');
$("#menu-item-2135").append('<div class="megamenu-wrap"><div class="megamenu optmenu-item-2135" /></div>');
$("#menu-item-2134").append('<div class="megamenu-wrap"><div class="megamenu optmenu-item-2134" /></div>');

$.each(opt2132, function(key, value){
    $(".optmenu-item-2132").append('<div class="column"><a href="'+value['link']+'"><img src="'+value['image']+'" /> <p><span>'+value['title']+'</span></p> </a></div>');
});
$.each(opt1916, function(key, value){
    $(".optmenu-item-1916").append('<div class="column"><a href="'+value['link']+'"><img src="'+value['image']+'" /> <p><span>'+value['title']+'</span></p> </a></div>');
});
$.each(opt2133, function(key, value){
    $(".optmenu-item-2133").append('<div class="column"><a href="'+value['link']+'"><img src="'+value['image']+'" /> <p><span>'+value['title']+'</span></p> </a></div>');
});
$.each(opt2072, function(key, value){
    $(".optmenu-item-2072").append('<div class="column"><a href="'+value['link']+'"><img src="'+value['image']+'" /> <p><span>'+value['title']+'</span></p> </a></div>');
});
$.each(opt2135, function(key, value){
    $(".optmenu-item-2135").append('<div class="column"><a href="'+value['link']+'"><img src="'+value['image']+'" /> <p><span>'+value['title']+'</span></p> </a></div>');
});
$.each(opt2134, function(key, value){
    $(".optmenu-item-2134").append('<div class="column"><a href="'+value['link']+'"><img src="'+value['image']+'" /> <p><span>'+value['title']+'</span></p> </a></div>');
});

$('.megamenu .column p').adjustHeight();

$(window).resize(function () {
    $('.megamenu .column p').adjustHeight();
});