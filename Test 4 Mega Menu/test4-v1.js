console.clear();

$ = jQuery.noConflict();

$('body').addClass('test4');

var navigation = {  
    "Front Door Solutions":[  
            {"name":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-french-solid-timber-with-glass/"},
            {"name":"Guardian 2in1 Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-guardian-2in1/"},
            {"name":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-hinged/"},
            {"name":"Large Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-large/"}
        ],
        "External Door Solutions":[  
            {"name":"Bi Fold Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-bi-fold/"},
            {"name":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-french-solid-timber-with-glass/"},
            {"name":"Guardian 2in1 Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-guardian-2in1/"},
            {"name":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-hinged/"},
            {"name":"Sliding & Stacking Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-sliding-stacking/"}
        ],
        "Internal Door Solutions":[  
            {"name":"Bi Fold Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-bi-fold/"},
            {"name":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-french-solid-timber-with-glass/"},
            {"name":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-hinged/"},
            {"name":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-sliding/"}
        ],
        "Zone Living Systems":[  
            {"name":"Bi - Fold Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-bi-fold/"},
            {"name":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-french/"},
            {"name":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-hinged/"},
            {"name":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-sliding-stacking/"}
        ],
        "Security Screen Door Solutions":[  
            {"name":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-hinged/"},
            {"name":"Large Doors","link":"https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-large/"},
            {"name":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-sliding/"}
        ],
        "Safety Screen Door Solutions":[  
            {"name":"Hideaway Retractable Screens Doors","link":"https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-hideaway-retractable-screens/"},
            {"name":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-hinged/"},
            {"name":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-sliding/"}
        ]
    };


function makeList(nav) {
    var html = '<ul class="sub-menu dropdown">'
    for (var x = 0; x < nav.length; x++) {
        var item = '<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="'+nav[x].link+'">'+nav[x].name+'</a>';
        html += item;
    }
    html += '</ul>';
    return html;
}

jQuery('#menu-main_menu > li').each(function () {
    var item = jQuery(this).text();
    console.log(navigation[item])
    if (navigation[item]) {
        var html = makeList(navigation[item]);
        jQuery(this).append(html);
    }
})
