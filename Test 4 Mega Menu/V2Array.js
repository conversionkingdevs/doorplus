var navigation = {  
    "Front Door Solutions":[  
            {"title":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-french-solid-timber-with-glass/","image":"//placehold.it/180x175"}
            {"title":"Guardian 2in1 Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-guardian-2in1/","image":"//placehold.it/180x175"}
            {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-hinged/","image":"//placehold.it/180x175"}
            {"title":"Large Doors","link":"https://www.doorsplus.com.au/doorsolutions/front/front-large/","image":"//placehold.it/180x175"}
        ],
        "External Door Solutions":[  
            1 : {"title":"Bi Fold Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-bi-fold/","image":"//placehold.it/180x175"}
            2 : {"title":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-french-solid-timber-with-glass/","image":"//placehold.it/180x175"}
            3 : {"title":"Guardian 2in1 Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-guardian-2in1/","image":"//placehold.it/180x175"}
            4 : {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-hinged/","image":"//placehold.it/180x175"}
            5 : {"title":"Sliding & Stacking Doors","link":"https://www.doorsplus.com.au/doorsolutions/external/external-sliding-stacking/","image":"//placehold.it/180x175"}
        ],
        "Internal Door Solutions":[  
            1 : {"title":"Bi Fold Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-bi-fold/","image":"//placehold.it/180x175"}
            2 : {"title":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-french-solid-timber-with-glass/","image":"//placehold.it/180x175"}
            3 : {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-hinged/","image":"//placehold.it/180x175"}
            4 : {"title":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/internal/internal-sliding/","image":"//placehold.it/180x175"}
        ],
        "Zone Living Systems":[  
            1 : {"title":"Bi - Fold Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-bi-fold/","image":"//placehold.it/180x175"}
            2 : {"title":"French (Solid Timber with Glass) Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-french/","image":"//placehold.it/180x175"}
            3 : {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-hinged/","image":"//placehold.it/180x175"}
            4 : {"title":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-sliding-stacking/","image":"//placehold.it/180x175"}
        ],
        "Security Screen Door Solutions":[  
            1 : {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-hinged/","image":"//placehold.it/180x175"}
            2 : {"title":"Large Doors","link":"https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-large/","image":"//placehold.it/180x175"}
            3 : {"title":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-sliding/","image":"//placehold.it/180x175"}
         ],
        "Safety Screen Door Solutions":[  
            1 : {"title":"Hideaway Retractable Screens Doors","link":"https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-hideaway-retractable-screens/","image":"//placehold.it/180x175"}
            2 : {"title":"Hinged Doors","link":"https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-hinged/","image":"//placehold.it/180x175"}
            3 : {"title":"Sliding Doors","link":"https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-sliding/","image":"//placehold.it/180x175"}
         ]
    };