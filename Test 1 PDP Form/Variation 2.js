function validatePhone(phone){
    var regex  = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    return regex.test(phone);
}
function validateEmail(email){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function validateName(name){
    if(name == "" || name == null){
        return false;
    } 
    return true;
}

function changeStep (step) {
    jQuery('.optShow').removeClass('optShow');

    switch (step) {
        case "Step1":
            jQuery('.optStep1').addClass('optShow');
            break;
        case "Step2":
            jQuery('.optStep2').addClass('optShow');
            break;
        default:
            break;
    }
}

// Add the right Form
jQuery('#form-right1').css('display','block');
jQuery('#form-right2').css('display','none');

jQuery('body').addClass('opt1');

var form = jQuery('#form-right1');

form.find('form').find('p:eq(0),p:eq(1),p:eq(2)').addClass('optStep1 optShow');
form.find('form').find('p:not(.optStep1), .col-md-12 > .wpcf7-form-control-wrap').addClass('optStep2');

form.find('form').find('.optStep1:eq(2)').after('<div class="optNext optStep1 optShow">Next Step</div>');

form.find('.wpcf7-submit').attr('value','Enquire about this Door Solution');

jQuery('.optNext').click(function () {
    passed = true;

    if (validateName(form.find('.enquiry-phone input').val()) == false) {
        passed = false;
    }

    if (validateName(form.find('.enquiry-name input').val()) == false) {
        passed = false;
    }

    if (validateName(form.find('.enquiry-email input').val()) == false) {
        passed = false;
    }

    if (passed == false) {
        form.find('.wpcf7-submit').submit();
    } else if (passed == true) {
        changeStep("Step2");
    }

});

