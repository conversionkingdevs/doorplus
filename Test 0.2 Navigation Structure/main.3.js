function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
defer(function(){
    console.clear();
    $ = jQuery.noConflict();
    if($(window).width() < 767){
        $('body').addClass('opt02-v3');    
    };
    var data = [
        '<ul> <li ><a href="https://www.doorsplus.com.au/doorsolutions/front/front-french-solid-timber-with-glass/" title="French (Solid Timber with Glass) Doors">French (Solid Timber with Glass) Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/front/front-guardian-2in1/" title="Guardian 2in1 Doors">Guardian 2in1 Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/front/front-hinged/" title="Hinged Doors">Hinged Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/front/front-large/" title="Large Doors">Large Doors<i class="fa fa-angle-right"></i></i></a></li></ul>',
        '<ul><li><a href="https://www.doorsplus.com.au/doorsolutions/external/external-bi-fold/" title="Bi Fold Doors">Bi Fold Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/external/external-french-solid-timber-with-glass/" title="French (Solid Timber with Glass) Doors">French (Solid Timber with Glass) Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/external/external-guardian-2in1/" title="Guardian 2in1 Doors">Guardian 2in1 Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/external/external-hinged/" title="Hinged Doors">Hinged Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/external/external-sliding-stacking/" title="Sliding &amp; Stacking Doors">Sliding &amp; Stacking Doors<i class="fa fa-angle-right"></i></i></a></li></ul>',
        '<ul><li><a href="https://www.doorsplus.com.au/doorsolutions/internal/internal-bi-fold/" title="Bi Fold Doors">Bi Fold Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/internal/internal-french-solid-timber-with-glass/" title="French (Solid Timber with Glass) Doors">French (Solid Timber with Glass) Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/internal/internal-hinged/" title="Hinged Doors">Hinged Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/internal/internal-sliding/" title="Sliding Doors">Sliding Doors<i class="fa fa-angle-right"></i></i></a></li></ul>',
        '<ul> <li><a href="https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-bi-fold/" title="Bi Fold Doors">Bi Fold Doors</a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-french/" title="French Doors">French Doors</a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-hinged/" title="Hinged Doors">Hinged Doors</a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/zone-living/zone-living-sliding-stacking/" title="Sliding/ Stacking Doors">Sliding/ Stacking Doors</a></li></ul>',
        '<ul><li><a href="https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-hinged/" title="Hinged Doors">Hinged Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-large/" title="Large Doors">Large Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/security-screen/security-screen-sliding/" title="Sliding Doors">Sliding Doors<i class="fa fa-angle-right"></i></i></a></li></ul>',
        '<ul><li><a href="https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-hideaway-retractable-screens/" title="Hideaway Retractable Screens Doors">Hideaway Retractable Screens Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-hinged/" title="Hinged Doors">Hinged Doors<i class="fa fa-angle-right"></i></i></a></li><li><a href="https://www.doorsplus.com.au/doorsolutions/safety-screen/safety-screen-sliding/" title="Sliding Doors">Sliding Doors<i class="fa fa-angle-right"></i></i></a></li></ul>'
    ];
    /**************************************
        LAYOUT V3
    **************************************/ 
    $(".opt02-v3 .head_area .col-md-12.main_nav .col-xs-6.sub_nav").prependTo($(".opt02-v3 .head_area .col-md-12.main_nav"));
    $(".opt02-v3").append('<div class="menu-overlay" ></div>');
    $(".opt02-v3").append('<aside class="sidebar-wrap"><div class="sidebar-buttons"><a class="back-sidebar-nav" href="#"><i class="fa fa-angle-left"></i> Back</a><a class="close-sidebar-nav" href="#"><i class="fa fa-times"></i></a></div><div class="sidebar-inner"></div></aside>');
    $(".opt02-v3 .sidebar-wrap .sidebar-inner").append('<nav class="sidebar-nav primary"><ul></ul></nav>');
    $(".opt02-v3 .sidebar-wrap .sidebar-inner").append('<nav class="sidebar-nav secondary"><ul></ul></nav>');
    $(".opt02-v3 .sidebar-nav.primary ul").html($(".opt02-v3 .mobi_nav_area .the_menu2 ul").html());
    $(".opt02-v3 .sidebar-nav.primary ul").removeClass('sub-menu');
    $(".opt02-v3 .sidebar-nav.secondary ul").html($(".opt02-v3 .head_area .the_menu").html());
    $(".opt02-v3 .sidebar-nav.secondary ul li").last().hide();
    $('<a id="search-button" href="#"><i class="fa fa-search"></i></a>').insertAfter($(".opt02-v3 #menu.m_show"));
    $(".opt02-v3 .search-form").wrap('<div class="search-form-wrap" ></div>');
    $(".opt02-v3 .search-form input[type='submit']").attr('value','');
    $(".opt02-v3 .sidebar-nav.primary ul > li").each(function(index){
        $this = $(this);
        if($this.find('ul').length > 0){
            $this.addClass('dropdown');
            $this.find('> a').append('<i class="fa fa-angle-right"></i>');
            $this.find('> ul').prepend('<li><a href="'+$this.find('> a').attr('href')+'">All '+$this.find('> a').text()+'</a></li>');
        } else {
            jQuery(this).append(data[index]);
        }
    });
    jQuery(".sidebar-wrap .sidebar-nav.primary li").not(".menu-item-has-children").addClass("dropdown").find("a").append('<i class="fa fa-angle-right"></i>');
    $(".opt02-v3 .sidebar-nav.primary > ul > li.dropdown > a").click(function(e){
        e.preventDefault();
        $this = $(this);
        $this.parent().find('ul').css('display','block');
        $(".opt02-v3 .sidebar-wrap .back-sidebar-nav").show();
        $(".opt02-v3 .sidebar-wrap .sidebar-inner").animate({marginLeft:-270});
    });
    $(".opt02-v3 .sidebar-wrap .close-sidebar-nav, .opt02-v3 .menu-overlay").click(function(e){
        e.preventDefault();
        $(".opt02-v3 .sidebar-wrap").animate({left:-270},300,function(){
            $(".opt02-v3 .sidebar-wrap .sidebar-nav.primary ul li ul").hide();
            $(".opt02-v3 .sidebar-wrap .sidebar-inner").css('margin-left','0');    
        });
        $(".opt02-v3 .menu-overlay").animate({opacity:0},300,function(){
            $(".opt02-v3 .menu-overlay").hide();    
        });
    });
    $(".opt02-v3 .sidebar-wrap .back-sidebar-nav").click(function(e){
        e.preventDefault();
        $(".opt02-v3 .sidebar-wrap .back-sidebar-nav").hide();
        $(".opt02-v3 .sidebar-wrap .sidebar-inner").animate({marginLeft:0},300,function(){
            $(".opt02-v3 .sidebar-wrap .sidebar-nav.primary ul li ul").hide();
        });
    });
    $(".opt02-v3 #menu.m_show").click(function(e){
        e.preventDefault();
        $(".opt02-v3 .sidebar-wrap").animate({left:0},300);
        $(".opt02-v3 .menu-overlay").css('display','block').animate({opacity:1},300);
    });
    $(".opt02-v3 #search-button").click(function(e){
        e.preventDefault();
        $this = $(this);
        if($this.hasClass('active')){
            $this.removeClass('active');
            $(".opt02-v3 .search-form-wrap").slideUp(200);
        } else {
            $this.addClass('active');
            $(".opt02-v3 .search-form-wrap").slideDown(200);
        };
    });
    $(".opt02-v3 .search-form input[type='search']").blur(function(){
        $(".opt02-v3 #search-button").removeClass('active');
        $(".opt02-v3 .search-form-wrap").slideUp(200);
    });
}, ".head_area .col-md-12.main_nav");
