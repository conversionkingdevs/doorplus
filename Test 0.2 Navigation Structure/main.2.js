function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function(){
console.clear();
$ = jQuery.noConflict();
if($(window).width() < 767){
    $('body').addClass('opt02-v2');    
};



/**************************************
    LAYOUT V2
**************************************/ 
$(".opt02-v2 .head_area .col-md-12.main_nav .col-xs-6.sub_nav").prependTo($(".opt02-v2 .head_area .col-md-12.main_nav"));
$(".opt02-v2").append('<div class="menu-overlay" />');
$(".opt02-v2").append('<aside class="sidebar-wrap"><div class="sidebar-buttons"><a class="back-sidebar-nav" href="#"><i class="fa fa-angle-left"></i> Back</a><a class="close-sidebar-nav" href="#"><i class="fa fa-times"></i></a></div><div class="sidebar-inner"></div></aside>');
$(".opt02-v2 .head_area .search-form").prependTo($(".opt02-v2 .sidebar-wrap .sidebar-inner"));
$(".opt02-v2 .sidebar-wrap .search-form input[type='submit']").attr('value','');
$(".opt02-v2 .sidebar-wrap .sidebar-inner").append('<nav class="sidebar-nav primary"><ul></ul></nav>');
$(".opt02-v2 .sidebar-wrap .sidebar-inner").append('<nav class="sidebar-nav secondary"><ul></ul></nav>');
$(".opt02-v2 .sidebar-nav.primary ul").html($(".opt02-v2 .mobi_nav_area .the_menu2 ul").html());
$(".opt02-v2 .sidebar-nav.primary ul").removeClass('sub-menu');
$(".opt02-v2 .sidebar-nav.secondary ul").html($(".opt02-v2 .head_area .the_menu").html());
$(".opt02-v2 .sidebar-nav.secondary ul li").last().hide();


$(".opt02-v2 .sidebar-nav.primary ul > li").each(function(){
    $this = $(this);
    if($this.find('ul').length > 0){
        $this.addClass('dropdown');
        $this.find('> a').append('<i class="fa fa-angle-right"></i>');
        $this.find('> ul').prepend('<li><a href="'+$this.find('> a').attr('href')+'">All '+$this.find('> a').text()+'</a></li>');
    };
});

$(".opt02-v2 .sidebar-nav.primary ul > li.dropdown > a").click(function(e){
    e.preventDefault();
    $this = $(this);
    $this.parent().find('ul').css('display','block');
    $(".opt02-v2 .sidebar-wrap .back-sidebar-nav").show();
    $(".opt02-v2 .sidebar-wrap .sidebar-inner").animate({marginLeft:-270});
});


$(".opt02-v2 .sidebar-wrap .close-sidebar-nav,.opt02-v2 .menu-overlay").click(function(e){
    e.preventDefault();
    $(".opt02-v2 .sidebar-wrap").animate({left:-270},300,function(){
        $(".opt02-v2 .sidebar-wrap .sidebar-nav.primary ul li ul").hide();
        $(".opt02-v2 .sidebar-wrap .sidebar-inner").css('margin-left','0');    
    });
    $(".opt02-v2 .menu-overlay").animate({opacity:0},300,function(){
        $(".opt02-v2 .menu-overlay").hide();    
    });
});


$(".opt02-v2 .sidebar-wrap .back-sidebar-nav").click(function(e){
    e.preventDefault();
    $(".opt02-v2 .sidebar-wrap .back-sidebar-nav").hide();
    $(".opt02-v2 .sidebar-wrap .sidebar-inner").animate({marginLeft:0},300,function(){
        $(".opt02-v2 .sidebar-wrap .sidebar-nav.primary ul li ul").hide();
    });
});


$(".opt02-v2 #menu.m_show").click(function(e){
    e.preventDefault();
    $(".opt02-v2 .sidebar-wrap").animate({left:0},300);
    $(".opt02-v2 .menu-overlay").css('display','block').animate({opacity:1},300);
});


}, ".head_area .col-md-12.main_nav");