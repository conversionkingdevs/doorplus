var test = {
    "https://www.doorsplus.com.au/doorsolutions/front/front-french-solid-timber-with-glass/" : [
        {
            "title": "<h2 class='opt-1 entry-title boxhead'>Solid Timber Door Package</h2>",
            "price" : "<p class='opt-1 boxbod'>From <span class='opt-1 bigyellow'>$2099</span> <br/>Package Includes:</p>",
            "includes": "<ul><li><span><img src=http://via.placeholder.com/23x23/></span>Hardware</li><li><span><img src=http://via.placeholder.com/23x23/></span>Delivery</li><li><span><img src=http://via.placeholder.com/23x23/></span>Installation</li></ul>"
        },
        {
            "title": "<div class='opt-1 sub-top'><h2>Guardian Door 2 in 1</h2></div>",
            "price": "<div class='opt-1 contmid'><img class='opt-1 sub-pic' src='https://www.doorsplus.com.au/wp-content/uploads/2016/05/Timber-with-Glass2-Front-external-119x300.png'/><div class='opt-1 sub-content'><span class='opt-1 from'>From</span> </br><span class='opt-1 bigyellow'>$1109</span> </br> Includes Hardware, Delivery and Installation. </br>Ready to Paint. </div></div>",
        },
        {
            "title": "<div class='opt-1 sub-top'><h2>Modern Timber Entrance Door</h2></div>",
            "price": "<div class='opt-1 contmid'><img class='opt-1 sub-pic' src='https://www.doorsplus.com.au/wp-content/uploads/2016/05/Timber-with-Glass2-Front-external-119x300.png'/><div class='opt-1 sub-content'><span class='opt-1 from'>From</span> </br><span class='opt-1 bigyellow'>$899</span> </br> Includes Hardware, Delivery and Installation. </br>Ready to Paint or Stain. </div></div>",
        }
    ],
 }
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
  }
var url = window.location.href;
var number = jQuery("#numdiv_17116_0").text();
defer(function(){
    
    console.log(test[url][0]["title"]);
    jQuery(".row .benefits").after("<h2 class='opt-1 entry-title thinner'>French Doors Package Options</h2><div class='opt-1 wrap'><div class='opt-1 lhs'></div><div class='opt-1 rhs'></div></div>");
    jQuery(".lhs").append("<div class='opt-1 incframe'>Includes Framework</div>");
    jQuery(".lhs").append("<img class='opt-1 picture' src='https://www.doorsplus.com.au/wp-content/uploads/2016/04/French-Door-2-handles.jpg'/>");
    
    jQuery(".rhs").append(test[url][0]["title"]);
    jQuery(".rhs").append(test[url][0]["price"]);
    jQuery(".rhs").append(test[url][0]["includes"]);
    jQuery(".rhs").append("<div class='opt-1 pnum'></div>");
    jQuery(".pnum").html("<a href='tel:" + number + "'><div class='opt-1 fone'> <span><img src=http://via.placeholder.com/23x23/></span>"+ number + "</div></a>");
    jQuery(".wrap").after("<h2 class='opt-1 entry-title thinner'>People Also Looked At:</h2><div class='opt-1 wrap2'><div class='opt-1 sub-button suba'></div><div class='opt-1 sub-button subb'></div></div>");
    jQuery(".sub-button").append("<div class='opt-1 nofuss'>No Fuss</div>");
    jQuery(".suba").append(test[url][1]["title"]);
    jQuery(".subb").append(test[url][2]["title"]);
    jQuery(".suba").append(test[url][1]["price"]);
    jQuery(".subb").append(test[url][2]["price"]);
    jQuery(".sub-button").append("<a><div class='opt-1 fone sub-bot'>VIEW INFO</div></a>");
    jQuery(".wrap2").after("<div class='opt-1 wrap3'><a><div id='opt-findstore' class='opt-1 bot-button suba'>Find In Store</div></a><a><div id='opt-enquire' class='opt-1 bot-button subb'>Enquire</div></a></div>");
    setInterval(function(){
        //Get number
        number = jQuery("#numdiv_17116_0").text();
        //Update phone number
        jQuery(".pnum").html("<a href='tel:" + number + "'><div class='opt-1 fone'> <span><img src=http://via.placeholder.com/23x23/></span>"+ number + "</div></a>");
    },2000);
  }, "#numdiv_17116_0");
