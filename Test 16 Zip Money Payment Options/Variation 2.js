function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function () {
    jQuery('.contact-front').before('<img style="margin-bottom:20px" class="opt_Image" src="//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/f2ddeb5c38916165f6bfe6e0fdef4388_banner.png" />');
},'.contact-front');