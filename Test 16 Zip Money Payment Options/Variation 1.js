function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function CVPs (newoptions) {

    /** DEFAULT OPTIONS */
    var default_options = {
        container: 'opt_CVPContainer',
        rotate: {
            enable: true,
            breakpoint: 520,
            interval: 2000,
            onresize: true
        },
        place: 'body',
        location: 'prepend',
        slides: [
            '<img src="//placehold.it/28x20" /><p>This is a Default 1<p>',
            '<img src="//placehold.it/96x20" /><p>This is a Default 2<p>',
            '<img src="//placehold.it/20x21" /><p>This is a Default 3<p>'
            ]
        };

    var options = newoptions || {};
    for (var opt in default_options) {
        if (default_options.hasOwnProperty(opt) && !options.hasOwnProperty(opt)) {
            options[opt] = default_options[opt];
        }
    }

    /** Initiate */
    this.initCVPs = function () {
        if (jQuery('#opt_CVPModule_css').length <= 0) {
            this.addCSS(); // Add CSS
        }

        this.addHTML(); // Add slides

        if (options.rotate.enable === true) { // Enable rotator?
            if (options.rotate.onresize === true) {
                var _this = this;
                this.enableRotate();
                jQuery(window).resize(function () {
                    _this.enableRotate();
                });
            } else {
                this.enableRotate();;
            }
        }
    };
    
    jQuery.fn.adjustHeight = function() {
        var maxHeightFound = 0;
        this.css('min-height','1px');
        this.each(function() {
            if( jQuery(this).height() > maxHeightFound ) {
                maxHeightFound = jQuery(this).height();
            }
        });
        this.css('min-height', maxHeightFound);
    };

    this.adjustHeight = function (state) {
        jQuery('.'+options.container).css('min-height','1px');
        if (state === true) {
            var maxHeightFound = 0;
            jQuery('.'+options.container+' .opt_CVPModule_slide').each(function() {
                if( jQuery(this).height() > maxHeightFound ) {
                    maxHeightFound = jQuery(this).height();
                }
            });
            jQuery('.'+options.container).css('min-height', maxHeightFound);
        } 
    };

    /** HTML */
    this.createSlides = function () {
        var html = '';
        for (var i = 0; i < options.slides.length; i++) {
            html += '<div class="opt_CVPModule_slide" id="slide_'+i+'">'+options.slides[i]+'</div>';
        }
        return html;
    };
    
    this.createHTML = function () {
        var html = '<div class="'+options.container+' opt_DefaultContainer">'+this.createSlides()+'</div>';
        return html;
    };
    
    this.addHTML = function () {
        switch (options.location) {
            case 'prepend':
                jQuery(options.place).prepend(this.createHTML());
                break;
            case 'append':
                jQuery(options.place).append(this.createHTML());
                break;
            case 'after':
                jQuery(options.place).after(this.createHTML());
                break;
            case 'before':
                jQuery(options.place).before(this.createHTML());
                break;
            default:
                break;
        }
    };

    /** Rotating */
    this.enableRotate = function () { // If options.rotate = true than we will enable rotate at the breakpoint
        var _this = this;
        if (!jQuery('body').hasClass('opt_CVPModule_rotate') && jQuery(window).width() <= options.rotate.breakpoint) {
            jQuery('body').addClass('opt_CVPModule_rotate');
            jQuery('.'+options.container+'.opt_DefaultContainer .opt_CVPModule_slide').removeClass('opt_CVPModule_active opt_CVPModule_previous opt_CVPModule_next');
            /** Could change to use a starting value from options default 0*/
            jQuery('.'+options.container+'.opt_DefaultContainer .opt_CVPModule_slide:eq(0)').addClass('opt_CVPModule_previous');
            jQuery('.'+options.container+'.opt_DefaultContainer .opt_CVPModule_slide:eq(1)').addClass('opt_CVPModule_active');
            jQuery('.'+options.container+'.opt_DefaultContainer .opt_CVPModule_slide:eq(2)').addClass('opt_CVPModule_next');
            this.rotatorInterval = setInterval(function () {
                _this.rotator();
            }, options.rotate.interval);
            this.adjustHeight(true);
            // SetInterval to go to next slide
        } else if ((jQuery('body').hasClass('opt_CVPModule_rotate') && jQuery(window).width() > options.rotate.breakpoint)) {
            if (this.rotatorInterval) {
                clearInterval(this.rotatorInterval);
                this.adjustHeight(false);
            }
            jQuery('.opt_CVPModule_rotate').removeClass('opt_CVPModule_rotate');
        }
    };

    this.rotator = function () { /** Rotates through slides can be manually called on the object*/
        var $container = jQuery('.opt_DefaultContainer.'+options.container),
            activeIndex = $container.find('.opt_CVPModule_active').index(),
            nextActiveSlide,
            nextNextSlide;
        /** Check end of slides */
        if (activeIndex + 1 > $container.find('.opt_CVPModule_slide').length - 1) {
            nextActiveSlide = 0;
            nextNextSlide = 1;
        } else  if (activeIndex + 2 > $container.find('.opt_CVPModule_slide').length - 1) {
            nextActiveSlide = activeIndex + 1;
            nextNextSlide = 0;
        } else {
            nextActiveSlide = activeIndex + 1;
            nextNextSlide = activeIndex + 2;
        }
        /** Assign new positions */
        $container.find('.opt_CVPModule_slide').removeClass('opt_CVPModule_active opt_CVPModule_previous opt_CVPModule_next');
        $container.find('.opt_CVPModule_slide:eq('+activeIndex+')').addClass('opt_CVPModule_previous');
        $container.find('.opt_CVPModule_slide:eq('+nextActiveSlide+')').addClass('opt_CVPModule_active');
        $container.find('.opt_CVPModule_slide:eq('+nextNextSlide+')').addClass('opt_CVPModule_next');

    };

    this.addCSS = function () {
        jQuery('head').prepend(' <style id="opt_CVPModule_css">.opt_DefaultContainer *{padding: 0; margin: 0;}.opt_DefaultContainer{display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-justify-content: space-around; -ms-flex-pack: distribute; justify-content: space-around; padding: 10px; background: #F5F5F5; max-width: 1440px;}@-webkit-keyframes active{0%{left: 100%; visibility: visible;}100%{display: inline-block; left: 0; visibility: visible;}}@keyframes active{0%{left: 100%; visibility: visible;}100%{display: inline-block; left: 0; visibility: visible;}}@-webkit-keyframes previous{0%{display: inline-block; left: 0; visibility: visible;}100%{left: -100%; visibility: visible;}}@keyframes previous{0%{display: inline-block; left: 0; visibility: visible;}100%{left: -100%; visibility: visible;}}@media (max-width: '+options.rotate.breakpoint+'px){/** Rotate CSS **/ .opt_CVPModule_rotate .opt_DefaultContainer{display: block; position: relative; width: 100%;box-sizing: content-box;}.opt_CVPModule_rotate .opt_DefaultContainer .opt_CVPModule_slide{width: 100%; display: inline-block; visibility: hidden; background: white; text-align: center; -webkit-transition-property: left, display, visibility; transition-property: left, display, visibility; -webkit-transition-duration: 1s; transition-duration: 1s; -webkit-transition-timing-function: linear; transition-timing-function: linear; position: absolute; -webkit-animation-timing-function: linear; animation-timing-function: linear; -webkit-animation-duration: 1s; animation-duration: 1s; -webkit-animation-iteration-count: 1; animation-iteration-count: 1;}.opt_CVPModule_rotate .opt_DefaultContainer .opt_CVPModule_active{left: 0; -webkit-animation: active; animation: active; visibility: visible;}.opt_CVPModule_rotate .opt_DefaultContainer .opt_CVPModule_previous{left: -100%; -webkit-animation: previous; animation: previous; visibility: hidden;}.opt_CVPModule_rotate .opt_DefaultContainer .opt_CVPModule_next{visibility: hidden; left: 100%;}}</style>');
    };

    /** Destroy */
    this.destroy = function () {

    };
}

defer(function () {
var cvpsoptionsDesktop = {
    container: 'opt_CVPContainer',
    rotate: {
        enable: false,
        breakpoint: 959,
        interval: 4000,
        onresize: false
    },
    place: '.opt_cvpcontainer',
    location: 'append',
    slides: [
        '<p><img class="opt_Image" src="https://static.zipmoney.com.au/assets/default/landing-page/img/heart.svg" /><strong>Want It</strong><img class="opt_Image" src="https://static.zipmoney.com.au/assets/default/landing-page/img/shoppingcart.svg" /><strong>Get it</strong><img class="opt_Image" src="https://static.zipmoney.com.au/assets/default/landing-page/img/thumb.svg" /><strong>Own It</strong></p>',
        '<p>up to <strong class="opt_MarginLeft"> 12 months interest free</strong></p>',
        '<a href="https://www.doorsplus.com.au/payment-options/"><img src="//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/26b2cd5c3542898da22c2d3f11d18a9e_zipmoney-logo.png" /><div class="opt_Button">Learn More</div></a>'
        ]
    };

    desktopcvps = new CVPs(cvpsoptionsDesktop);

    jQuery('.nav_area').after('<div class="opt_cvpcontainer"></div>');

  desktopcvps.initCVPs();
  jQuery('head').append('<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900" rel="stylesheet">');


    function checkScroll () {
        if (jQuery(window).width() < 690 && !jQuery('body').hasClass('opt_stickyit') && jQuery(window).scrollTop() >= jQuery('.moblogocon .logo-nav').offset().top + jQuery('.moblogocon .logo-nav').height()) {
            jQuery('body').addClass('opt_stickyit');
        } else if (jQuery('body').hasClass('opt_stickyit') && jQuery(window).scrollTop() < jQuery('.moblogocon .logo-nav').offset().top + jQuery('.moblogocon .logo-nav').height()) {
            jQuery('body').removeClass('opt_stickyit');
        }
    }
  jQuery(window).scroll(function () {
    checkScroll();
  })
  checkScroll();
},'.nav_area');