function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function(){
    var variation = 1;
    var difference = 1;
    jQuery("body").addClass("optv"+variation+"d"+difference);
    jQuery("body").addClass("optv"+variation);
    var variationOneImgs = [
        "//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/430a464de5535fba5a1769912757e0d6_speak_to_our_friendly_staff_&_get_expert_advice_yellow.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/cf70e169210cf6cc7923e2b992f8b572_speak_to_our_friendly_staff_&_get_expert_adviceblue.png",
    ]

    var variationTwoImgs = [
        [
            "//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/ef0b4d60f2a1ae404177d173320119a4_expertyellow.png",
            "//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/47be7e837dcbc8abf020a1d00f498cbd_measure_yellow.png",
            "//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/1fcb4e8db324db021a624b28e094443b_painting_yellow.png"
        ],
        [
            "//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/9322fd59d6ea73bd2c5196949866599f_expert_blue.png",
            "//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/b496107e4cfcb4cf38519998d22227f6_measure_blue.png",
            "//useruploads.visualwebsiteoptimizer.com/useruploads/312453/images/9c166dc33a0239181687891a755a2ffd_painting_blue.png"
        ]
    ];

    switch(variation){
        case 0:
            var img = variationOneImgs[difference];
            jQuery(".main_nav .sub_nav").append('<div class="opt-img-container opt-desktop"><img src="'+img+'" alt=""/></div>');
            jQuery(".head_area").after('<div class="opt-img-container opt-mobile"><img src="'+img+'" alt=""/></div>');
            break;
        case 1:
            if(difference == 0){
                var imgs = variationTwoImgs[difference];
                jQuery(".head_area").after('<div class="opt-cvp-container"> <div class="container"><div class="row"><div class="opt-img opt-active"><img src="'+imgs[0]+'" alt=""></div> <div class="opt-img"><img src="'+imgs[1]+'" alt=""></div> <div class="opt-img"><img src="'+imgs[2]+'" alt=""></div></div></div></div>');            
                var count = 1;
                var maxCount = 3
                setInterval(function(){
                    jQuery(".opt-active").removeClass("opt-active");
                    jQuery(".opt-img:eq("+count+")").addClass("opt-active");
                    if(count == maxCount - 1) {
                        count = 0;
                    } else {
                        count++;
                    }
                }, 2000);
            } else {
                var imgs = variationTwoImgs[difference];
                jQuery(".mobi_nav_area ").after('<div class="opt-cvp-container"> <div class="container"><div class="row"><div class="opt-img opt-active"><img src="'+imgs[0]+'" alt=""></div> <div class="opt-img"><img src="'+imgs[1]+'" alt=""></div> <div class="opt-img"><img src="'+imgs[2]+'" alt=""></div></div></div></div>');            
                var count = 1;
                var maxCount = 3
                setInterval(function(){
                    jQuery(".opt-active").removeClass("opt-active");
                    jQuery(".opt-img:eq("+count+")").addClass("opt-active");
                    if(count == maxCount - 1) {
                        count = 0;
                    } else {
                        count++;
                    }
                }, 2000); 
            }
            
            break;
    }
}, "#menu, #menu2");