function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function () {
    jQuery('body').addClass('opt5-v1');

    /**************************************
        LAYOUT V1
    **************************************/
    jQuery(".opt5-v1 .store_subbanner").wrap('<div class="store_subbanner_wrap" />');
    if(jQuery(window).width() > 767){
        jQuery(".opt5-v1 .wpsl-search").appendTo(jQuery(".store_subbanner_wrap"));
    };
    jQuery(".opt5-v1  .breadcrumb_area .container").append('<div class="search-message"><p>Would you like to search a different postcode?</p><a href="#">Search again</a></div>');

    var keep_top; 
    jQuery(document).ajaxComplete(function(){
        keep_top = setInterval(function(){
            jQuery("html, body").clearQueue().stop();
        },20);
        setTimeout(function(){clearInterval(keep_top)},3000);
            
        if(jQuery(".opt5-v1 #wpsl-stores li.no-results").length == 0){        
            jQuery(".search-message").css('display','block');
            jQuery(".store_subbanner_wrap").slideUp(300,function(){
                jQuery(".search-message").animate({opacity:1},250);    
            });
            if(jQuery(window).width() <= 767){
                jQuery(".opt5-v1").addClass('submited');
                jQuery(".opt5-v1 .wpsl-search .store-head").hide();
            };
        } else {
            if(jQuery(window).width() > 767){
                clearInterval(keep_top)
            };
        };
    });

    jQuery("body").on('click','.search-message a',function(e){
        e.preventDefault();
        jQuery(".search-message").css('display','none').css('opacity','0');
        jQuery(".store_subbanner_wrap").slideDown(300);
        jQuery("#wpsl-search-input").focus();
    });
    var currentWidth = jQuery(window).width();
    jQuery(window).resize(function () {
        if (currentWidth <= 767) {
            if (jQuery(window).width() > 767) {
                window.location.reload();
            }
        } else if (currentWidth > 767) {
            if (jQuery(window).width() <= 767) {
                window.location.reload();
            }
        }

    });
},'.store_subbanner');