function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

/**************************************
    LAYOUT V2
**************************************/
defer(function () {
    jQuery('body').addClass('opt5-v2');
    jQuery(".opt5-v2 .store_subbanner").wrap('<div class="store_subbanner_wrap" />');
    if(jQuery(window).width() > 767){
        jQuery(".opt5-v2 .wpsl-search").appendTo(jQuery(".store_subbanner_wrap"));
    };
    jQuery(".opt5-v2  .breadcrumb_area .container").append('<div class="search-message"><p>Would you like to search a different postcode?</p><a href="#">Search again</a></div>');

    var keep_top; 
    jQuery(document).ajaxComplete(function(){
        keep_top = setInterval(function(){
            jQuery("html, body").clearQueue().stop();
        },20);
        setTimeout(function(){clearInterval(keep_top)},3000);
            
        if(jQuery(".opt5-v2 #wpsl-stores li.no-results").length == 0){        
            jQuery(".search-message").css('display','block');
            jQuery(".store_subbanner_wrap").slideUp(300,function(){
                jQuery(".search-message").animate({opacity:1},250);    
            });
            
            jQuery(".opt5-v2 #wpsl-stores li").each(function(){
                $li = jQuery(this);
                if($li.find('.buttons').length == 0){
                    $li.find('.store_add > div').wrapAll('<div class="buttons" />');
                    $li.find('.buttons').insertAfter($li.find('.store_add'));    
                };
            });
            
            jQuery(".opt5-v2 #wpsl-gmap").css('max-height',jQuery(".opt5-v2 #wpsl-result-list").height() - 29);
            
            if(jQuery(window).width() <= 767){
                jQuery(".opt5-v2").addClass('submited');
                jQuery(".opt5-v2 .wpsl-search .store-head").hide();
                
                jQuery(".opt5-v2 #wpsl-stores li").each(function(){
                    $li = jQuery(this);
                    $li.find('.store_img').insertAfter($li.find('.store_add'));
                });
            };
        } else {
            if(jQuery(window).width() > 767){
                clearInterval(keep_top)
            };
        };
    });

    jQuery("body").on('click','.search-message a',function(e){
        e.preventDefault();
        jQuery(".search-message").css('display','none').css('opacity','0');
        jQuery(".store_subbanner_wrap").slideDown(300);
        jQuery("#wpsl-search-input").focus();
    });
    var currentWidth = jQuery(window).width();
    jQuery(window).resize(function () {
        if (currentWidth <= 767) {
            if (jQuery(window).width() > 767) {
                window.location.reload();
            }
        } else if (currentWidth > 767) {
            if (jQuery(window).width() <= 767) {
                window.location.reload();
            }
        }
    });
}, '.store_subbanner');